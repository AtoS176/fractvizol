function createCanopyTree(canvasId, iterations, deltaAngle){
    let canvas = document.getElementById(canvasId);
    let ctx = canvas.getContext('2d');

    let x1 = canvas.width / 2;
    let y1 = canvas.height - 10;

    const startAngle = -90;
    const rad = Math.PI / 180;
    const scale = calculateScale();


    function calculateScale(){
        let treeLength = canvas.height / iterations;
        let treeWidth = 0.5 * canvas.width / iterations;
        return Math.min(treeWidth, treeLength) / Math.ceil(iterations / 2);
    }

    function drawLine(x1, y1, x2, y2, iterations){
        choseLineWidth(iterations);
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
        ctx.closePath();
    }

    function choseLineWidth(iterations){
        if(iterations > 10){
            ctx.lineWidth = 12;
            ctx.strokeStyle = 'rgb(43,20,1)';
        }else if(iterations > 8){
            ctx.lineWidth = 8;
            ctx.strokeStyle = 'rgb(76,39,5)';
        }else if(iterations > 6){
            ctx.lineWidth = 5;
            ctx.strokeStyle = 'rgb(6,52,0)';
        }else if (iterations > 4){
            ctx.lineWidth = 2;
            ctx.strokeStyle = 'rgb(11,90,11)';
        }else{
            ctx.lineWidth = 1;
            ctx.strokeStyle = 'rgb(42,201,42)';
        }
    }

    async function drawing(x1, y1, angle, iterations) {
        if (iterations !== 0) {
            let x2 = x1 + (Math.cos(angle * rad) * iterations * scale);
            let y2 = y1 + (Math.sin(angle * rad) * iterations * scale);
            drawLine(x1, y1, x2, y2, iterations);
            await new Promise(r => setTimeout(r, 200));
            drawing(x2, y2, angle - deltaAngle, iterations - 1);
            drawing(x2, y2, angle + deltaAngle, iterations - 1);
        }
    }

    drawing(x1, y1, startAngle, iterations);
}

export default createCanopyTree();
