function createBarnsleyFern(canvasId, iterations, color){
    let canvas = document.getElementById(canvasId);
    let ctx = canvas.getContext('2d');

    let x = 0;
    let y = 0;


    function f1(x, y) {
        return [0.85 * x + 0.04 * y, - 0.04 * x + 0.85 * y + 1.6];
    }

    function f2(x, y){
        return [-0.15 * x + 0.28 * y, 0.26 * x + 0.24 * y + 0.44];
    }

    function f3(x, y){
        return [0.2 * x - 0.26 * y, 0.23 * x + 0.22 * y + 1.6];
    }

    function f4(y) {
        return [0, 0.016 * y];
    }

    function drawFragment() {
        let nextX, nextY;
        let probability = Math.random();

        if(probability < 0.01){
            [nextX, nextY] = f4(y);
        }else if(probability < 0.86){
            [nextX, nextY] = f1(x, y);
        }else if(probability < 0.93){
            [nextX, nextY] = f2(x, y);
        }else{
            [nextX, nextY] = f3(x, y);
        }

        let centerX = canvas.width * (x + 3) / 6;
        let centerY = canvas.height - canvas.height * ((y + 2) / 14);

        drawCircle(centerX, centerY);

        [x, y] = [nextX, nextY];
    }

    function drawCircle(x, y) {
        ctx.fillStyle = color;
        ctx.fillRect(x, y, 2, 3);
    }

    async function drawFern(){
        for(let i = 0; i < iterations; i++){
            drawFragment();
            await new Promise(r => setTimeout(r, 1))
        }
    }

    drawFern();
}

export default createBarnsleyFern();