function createBurningShip(canvasId, iterations){
    let canvas = document.getElementById(canvasId);
    let ctx = canvas.getContext('2d');

    const inf = 4.0
    const scale = 15;
    const [minX, maxX] = [-28.5, -25];
    const [minY, maxY] = [-1.5, 0.5];

    let dx = (maxX - minX) / canvas.width;
    let dy = (maxY - minY) / canvas.height;


    function drawBurningShip() {
        let x = minX;
        for (let i = 0; i < canvas.width; i++) {
            let y = minY;
            for (let j = 0; j < canvas.height; j++) {
                let iterationsNum = checkLimes(x/scale, y/scale);
                ctx.fillStyle = getColor(iterationsNum%255);
                ctx.fillRect(i, j, 1, 1);
                y+= dy;
            }
            x += dx;
        }
    }

    function checkLimes(x, y){
        let n = 0;
        let a = x;
        let b = y;
        while(n < iterations && a * a + b * b < inf){
            let tmp = a * a - b * b + x;
            b = Math.abs(2 * a * b) + y;
            a = tmp;
            n++;
        }
        return n;
    }

    function getColor(n) {
        n*=4;
        return "rgb(" +n+ "," +n/5+ ","+n/7+")";
    }

    drawBurningShip();
}

export default createBurningShip();

