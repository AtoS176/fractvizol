function createPythagorasTree(canvasId, iterations, alfa) {
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext('2d');

    const beta = Math.PI / 2 - alfa;
    const scaleAlfa = Math.cos(alfa);
    const scaleBeta = Math.cos(beta);
    const size = calculateSize();


    function calculateSize(){
        let maxQ = Math.max(scaleAlfa, scaleBeta);
        return canvas.height / Math.ceil((1 - Math.pow(maxQ, iterations)) / (1 - maxQ));
    }

    async function drawPythagorasTree() {
        let left = (canvas.width) / 2 - size/4;
        let top = canvas.height - size;

        ctx.translate(left, top);

        for (let i = 0; i < iterations; i++) {
            drawFragment(i, (15 * i) % 255);
            await new Promise(r => setTimeout(r, 100));
        }
    }

    function drawFragment(step, color) {
        if (step === 0) {
            drawSquare(color);
        } else {
            ctx.save();
            ctx.translate(0, 0);
            ctx.scale(scaleAlfa, scaleAlfa);
            ctx.rotate(-alfa);
            ctx.translate(0, -size)
            drawFragment(step - 1, color)
            ctx.restore()

            ctx.save();
            ctx.translate(size, 0);
            ctx.scale(scaleBeta, scaleBeta);
            ctx.rotate(beta);
            ctx.translate(-size, -size)
            drawFragment(step - 1, color)
            ctx.restore()
        }
    }

    function drawSquare(n) {
        ctx.fillStyle = "rgb(" + n/3 + "," + n + "," + 10 + ")";
        ctx.fillRect(0, 0, size, size);
    }

    drawPythagorasTree();
}

export default createPythagorasTree();
