function createJuliaSet(canvasId, iterations, a, bi) {
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext('2d');

    const width = canvas.width;
    const height = canvas.height;

    const c = {
        a: a,
        b: bi
    };

    const inf = 16.0;
    const [minX, maxX] = [-2,2];
    const [minY, maxY] = [-2,2];

    const dx = (maxX - minX) / width;
    const dy = (maxY - minY) / height;


    function drawJuliaSet() {
        let y = minY;
        for (let i = 0; i < height; i++) {
            let x = minX;
            for (let j = 0; j < width; j++) {
                let iterations = checkLimes(x, y);
                ctx.fillStyle = getColor(iterations%256);
                ctx.fillRect(j, i, 1, 1);
                x += dx;
            }
            y += dy;
        }
    }

    function checkLimes(x, y) {
        let n = 0;
        let a = x;
        let b = y;
        while (n < iterations) {
            let aSquare = a * a;
            let bSquare = b * b;
            let newA = aSquare - bSquare + c.a;
            b = 2.0 * a * b + c.b;
            a = newA;
            if (aSquare * aSquare + bSquare * bSquare > inf) {
                return n;
            }
            n++;
        }
        return n;
    }

    function getColor(n) {
        return "rgb(" + n + "," + n/3 + "," +0+ ")";
    }

    drawJuliaSet()
}

export default createJuliaSet();
