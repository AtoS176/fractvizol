<h2>FractVizol</h2>
<p>FractVizol is javascript library to draw popular fractals.</p>

<h3>Available Fractals</h3>
<div>
<ul>
<li>
<div>
<b>Barnseley Fern</b> <br>
Fucntion : createBarnsleyFern(canvasId, iterations, color) <br>
Example : createBarnsleyFern('canvas', 100000, 'rgb(120,170, 10)') <br>
<img src="images/barnsleyFern.png" alt="Barnsley Fern">
</div>
</li>
<li>
<div>
<b>Burning Ship </b> <br>
Function :  createBurningShip(canvasId, iterations) <br>
Example :  createBurningShip('canvas', 100) <br>
<img src="images/burningShip.png" alt="Burning Ship">
</div>
</li>
<li>
<div>
<b>Canopy Tree </b><br>
Function : createCanopyTree(canvasId, iterations, deltaAngle) <br>
Example : createCanopyTree('canvas', 14, 25) <br>
<img src="images/canopyFractal.png" alt="Canopy Tree">
</div>
</li>
<li>
<div>
<b>Julia Set</b> <br>
Function : createJuliaSet(canvasId, iterations, a, bi) <br>
Example : createJuliaSet('canvas', 10000, -0.79, 0.18) <br>
<img src="images/juliaSet.png" alt="Julia Set">
</div>
</li>
<li>
<div>
<b>Pythagoras Tree </b><br>
Function : createPythagorasTree(canvasId, iterations, alfa) <br>
Example : createPythagorasTree('canvas', 12, 0.6) <br>
<img src="images/pythagorasTree.png" alt="Pythagoras Tree">
</div>
</li>
</ul>
</div>